// VARIABLES

const subTotalAmountInput = document.getElementById("subtotal-amount");
const peopleNumberInput = document.getElementById("people-number");
const resultTable = document.getElementById("result-table");
const calcButton = document.getElementById("calculate");
const paymentEquallyRadio = document.getElementById("equally");
const paymentSeparatelyRadio = document.getElementById("separately");
const paymentEquallyView = document.getElementById("payment-equally");
const paymentSeparatelyView = document.getElementById("payment-separately");
const nameInput = document.getElementById("name-input");
const addPersonBtn = document.getElementById("add-person-btn");
const itemTitleInput = document.getElementById("item-title");
const itemPriceInput = document.getElementById("item-price");
const addItemBtn = document.getElementById("add-item-btn");
const peopleDataTable = document.getElementById("people-data");
const peopleNamesSelect = document.getElementById("people-names-select");
const tipsPercentageInput = document.getElementById("tip-percentage");
const tipsAmountInput = document.getElementById("tip-amount");

const selectBaseContent = '<option value="--" selected>--</option>';

let resultData = [];
let peopleData = [];


// FUNCS

const clearResTable = () => {
    resultTable.tBodies[0].innerHTML = '';
}

const clearPeopleTable = () => {
    peopleDataTable.innerHTML = '';
}

const clearTipInputs = () => {
    tipsAmountInput.value = 0;
    tipsPercentageInput.value = 0;
}

/**
 *
 * @param {float} amount
 */
const calcTips = (amount) => {
    if (parseFloat(tipsPercentageInput.value) > 0) {
        tipsAmountInput.value = parseFloat((parseFloat(tipsPercentageInput.value) * amount / 100).toFixed(2));
    } else if (parseFloat(tipsAmountInput.value) > 0) {
        tipsPercentageInput.value = parseInt(parseFloat(tipsAmountInput.value) / amount * 100);
    }
    resultData.push(["Tips", tipsPercentageInput.value + "% = $" + tipsAmountInput.value]);
}

/**
 *
 * @param {float} subTotalAmount
 * @param {int} peopleNumber
 * @returns {int}
 */
const calcEqually = (subTotalAmount, peopleNumber) => {
    return parseFloat((subTotalAmount / peopleNumber).toFixed(2));
}

const calcSeparately = (personTipShare) => {
    let personItemsSum = [];
    for (const person of peopleData) {
        let sum = 0.0;
        for (const item of person.items) {
            sum += parseFloat((parseFloat(item.price)).toFixed(2));
        }
        personItemsSum.push([person.name, parseFloat((sum + personTipShare).toFixed(2))]);
    }
    return personItemsSum;
}

const updateResultDataTable = () => {
    clearResTable();
    let tableContent = "";
    for (const item of resultData) {
        tableContent += `<tr><td>${item[0]}</td><td>${item[1]}</td></tr>`;
    }
    resultTable.tBodies[0].insertAdjacentHTML('beforeend', tableContent);
    resultData = [];
}

const updatePeopleTable = () => {
    clearPeopleTable();
    let tableContent = "";
    for (const person of peopleData) {
        let tableItems = "";
        if (person.items.length != 0) {
            for (item of person.items) {
                tableItems += `<tr><td>-${item.title}</td><td>${item.price}</td></tr>`;
            }
        }
        tableContent += `<tbody><tr><td>${person.name}</td><td></td></tr>${tableItems}</tbody>`;
    }
    // peopleDataTable.innerHTML = tableContent;
    peopleDataTable.insertAdjacentHTML('beforeend', tableContent);
}

const updatePeopleNamesSelect = () => {
    peopleNamesSelect.innerHTML = '';
    let selectContent = selectBaseContent;
    for (const person of peopleData) {
        selectContent += `<option value="${person.name}" selected>${person.name}</option>`;
    }
    // peopleNamesSelect.innerHTML = selectContent;
    peopleNamesSelect.insertAdjacentHTML('beforeend', selectContent);
}

const addPerson = () => {
    peopleData.push({
        name: nameInput.value,
        items: [],
    });
    updatePeopleTable();
    updatePeopleNamesSelect();
    nameInput.value = '';
}

const addPersonItem = () => {
    let personName = peopleNamesSelect.value;
    if (personName) {
        for (const person of peopleData) {
            if (person.name === personName) {
                person.items.push({"title": itemTitleInput.value, "price": itemPriceInput.value});
                break;
            }
        }
    }
    updatePeopleTable();
}

const calculateSubTotalSum = () => {
    let sum = 0.0;
    for (const person of peopleData) {
        for (const item of person.items) {
            sum += parseFloat((parseFloat(item.price)).toFixed(2));
        }
    }
    return parseFloat(sum.toFixed(2));
}


// EVENTS

calcButton.addEventListener("click", () => {
    if (paymentEquallyRadio.checked) {
        if (subTotalAmountInput.checkValidity() && peopleNumberInput.checkValidity()) {
            let subTotalAmountValue = parseFloat(subTotalAmountInput.value);
            if (tipsPercentageInput.checkValidity() || tipsAmountInput.checkValidity()) {
                calcTips(subTotalAmountValue);
            }
            let personBillShare = calcEqually(parseFloat(tipsAmountInput.value) + subTotalAmountValue, parseInt(peopleNumberInput.value));
            resultData.push(["Bill share", personBillShare]);
            updateResultDataTable();
        }
    } else {
        let personTipShare = 0;
        let subTotalAmountValue = calculateSubTotalSum();
        resultData.push(["Sub total amount", subTotalAmountValue]);
        if (tipsPercentageInput.checkValidity() || tipsAmountInput.checkValidity()) {
            calcTips(subTotalAmountValue);
            personTipShare = calcEqually(parseFloat(tipsAmountInput.value), peopleData.length);
            resultData.push(["Tip share", personTipShare]);
        }
        resultData = resultData.concat(calcSeparately(personTipShare));
        updateResultDataTable();
    }
    clearTipInputs();
});

paymentEquallyRadio.addEventListener('click', () => {
    paymentSeparatelyView.hidden = true;
    paymentEquallyView.hidden = false;

});

paymentSeparatelyRadio.addEventListener('click', () => {
    paymentEquallyView.hidden = true;
    paymentSeparatelyView.hidden = false;
});

addPersonBtn.addEventListener('click', () => {
    if (nameInput.value != '' && nameInput.checkValidity()) {
        addPerson();
    }
});

addItemBtn.addEventListener('click', () => {
    if (peopleNamesSelect.value != "--" && itemTitleInput.value != '' && itemPriceInput.value != '') {
        addPersonItem();
    }
});